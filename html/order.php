<?php
    //This is the order page for pizzas
    $title = "Pizzas";
    require("../views/header.php");
    $menu=simplexml_load_file("../menu/menu.xml");
    //print_r($menu);

    foreach($menu->pizzas->pizza as $pizza)
    {
        echo $pizza;
        $name = $pizza;
        $small_price = number_format($pizza->price->small / 100, 2);
        $large_price = number_format($pizza->price->large /100, 2);

        echo "<form method='post' action='cart.php'>";
        echo "<input type='hidden' name='name' value='". $name . "'>";
        echo "<button type='submit' name='price' value='" . $small_price . "'> Order small: $" .$small_price ." </button>";
        echo "<button type='submit' name='price' value='" . $large_price . "'> Order large: $" .$large_price ." </button>";
        echo "</form>";

        echo '<hr>';
    }

    require("../views/footer.php");
?>
