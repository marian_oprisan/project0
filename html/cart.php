<div>
    <h1>Cart contents</h1>
    <?php
    session_start();
    require ("../helpers/cart.php");

    if (isset($_POST['remove']))
    {
        session_destroy();
        unset($_POST['remove']);
        if (showtotal() < 1)
        {
            echo "There are no items in your shopping cart. Click <a href='order.php'>here</a> to order.";
        }
        else
        {
            echo "Thank you for ordering! Click <a href='index.php'>here</a> to go to the main page.";
            echo "<br>Total: $" . showtotal();
        }
        checkout();
    }
    
    if (isset($_POST['name']) && isset($_POST['price']))
    {
        addtocart($_POST['name'], $_POST['price']);
        unset($_POST['name']);
        unset($_POST['price']);

        showcart();
    }

    ?>
    
    <form action='cart.php' method='post'>
      <input type='submit' name='remove' value='Checkout'>
    </form>
</div>
