<?php
    //Helper functions
    
    function addtocart($product,$price)
    {  
        if(isset($_SESSION['cart']))
        {
            $max=count($_SESSION['cart']);
            $_SESSION['cart'][$max]['name']=$product;
            $_SESSION['cart'][$max]['price']=$price;
        }
        else
        {
            $_SESSION['cart']=array();
            $_SESSION['cart'][0]['name']=$product;
            $_SESSION['cart'][0]['price']=$price;
        }
    }

    function showcart()
    {
        if(isset($_SESSION['cart'])) 
        {
            $max = count($_SESSION['cart']);
            for($i = 0; $i < $max; $i++)
            {
                echo '<p>';
                echo $_SESSION['cart'][$i]['name'];
                echo '</p>';
                echo 'price:'. $_SESSION['cart'][$i]['price'];
                echo '<br />';
            }
        }
        else
        {
            return;
        }
    }

    function removeitem($product)
    {
        if(isset($_SESSION['cart']))
        {
            $max = count($_SESSION['cart']);
            for($i = 0; $i < $max; $i++)
            {
                if($_SESSION['cart'][$i]['name'] == $product)
                {
                    unset($_SESSION['cart'][$i]);
                }
            }
            $_SESSION['cart'] = array_values($_SESSION['cart']);
            showcart();
        }
    }

    function showtotal()
    {
        if (isset($_SESSION['cart']))
        {
            $total = 0;
            $elements = count($_SESSION['cart']);
        
            for($i = 0; $i < $elements; $i++)
            {
               $total = $total + $_SESSION['cart'][$i]['price']; 
            }

            if ($total > 1)
            {
                return number_format($total, 2);
            }
            
            
        }
        else
        {
            return 0;
        }

    }

    function checkout()
    {
        if(isset($_SESSION['cart'])) 
        {
            $max = count($_SESSION['cart']);
            for($i = 0; $i < $max; $i++)
            {
                unset($_SESSION['cart'][$i]['name']);
                unset( $_SESSION['cart'][$i]['price']);
            }
        }
        else
        {
            return;
        }
    }

?>
