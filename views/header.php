<?php
    //This is the header file for the PizzaML page
    require("../helpers/cart.php");
?>
    <!DOCTYPE html>
    <html>
      <head>
        <?php
        session_start();

        if (isset($title))
        {
             print("<title>$title</title>");
        }
        else
        {
             print("<title>PizzaML</title>");
        }?>
         <!--  <link rel="stylesheet" type="text/css" href="../CSS/stylesheet.css"> -->
         <style>
            <?php require("../CSS/stylesheet.css");?>
         </style>
      </head>
      <body>
      <div id="static_top">
        <p>Three Aces</p>
        <hr>
        <form action='cart.php' method='post'>
            <button name='See cart contents' type='submit' value='See cart contents'>See cart contents</button>
        </form>
      </div>
